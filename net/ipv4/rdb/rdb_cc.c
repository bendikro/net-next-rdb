/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2012-2015, Bendik Rønning Opstad <bro.devel+kernel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <net/tcp.h>

#include "rdb.h"
#include "rdb_cc.h"
#include "rdb_skb.h"
#include "lib/rdb_tfrc.h"

/**
 * rdb_reset_cong() - Reset the given RDBcong struct
 * @sk: the socket.
 */
static inline void rdb_reset_cong(struct sock *sk)
{
	struct RDBcong *cong = inet_csk_ca(sk);
	cong->rdb_ack_loss_count = 0;
	cong->rdb_ack_loss_count_total = 0;
	cong->dynamic_pif = dynamic_pif;
	cong->minrtt_us = tcp_sk(sk)->srtt_us;
	cong->prior_in_flight = 0;
	cong->cc_mode = CC_UNINIT;
}

/**
 * rdb_cong_tfrc_init() - Initialize RDB TFRC cong on new connection
 * @sk: the socket.
 */
void rdb_cong_tfrc_init(struct sock *sk)
{
	struct RDBcong *cong = inet_csk_ca(sk);
	if (rdb_tfrc_cong_init(&cong->tfrc)) {
		pr_info("rdbcong: Failed to initialize!\n");
		return;
	}
	rdb_reset_cong(sk);
	cong->cc_mode = CC_TFRCSP;
}

/**
 * rdb_cong_tfrc_release() - Release resources when connection closes
 * @sk: the socket.
 */
void rdb_cong_tfrc_release(struct sock *sk)
{
	struct RDBcong *cong = inet_csk_ca(sk);
	if (WARN_ON(cong->cc_mode != CC_TFRCSP)) {
	}
	else {
		cong->cc_mode = CC_UNINIT;
		rdb_tfrc_cong_exit(&cong->tfrc);
	}
}

/**
 * rdb_cong_tfrc_init() - Initialize RDB TFRC cong on new connection
 * @sk: the socket.
 */
void rdb_cong_init(struct sock *sk)
{
	struct RDBcong *cong = inet_csk_ca(sk);
	rdb_reset_cong(sk);
	cong->cc_mode = CC_DEFAULT;
}

/**
 * rdb_cong_tfrc_release() - Release resources when connection closes
 * @sk: the socket.
 */
void rdb_cong_release(struct sock *sk)
{
	struct RDBcong *cong = inet_csk_ca(sk);
	cong->cc_mode = CC_UNINIT;
}


/**
 * rdb_check_rtx_queue_acked() - Perform loss detection by analysing acks.
 * @sk: the socket.
 * @seq_acked: The sequence number that was acked.
 * @acked: The sk_buff which will be set to the SKB (with highest seqnum) that was acked.
 * @lost: The sk_buff which will be set to the first SKB (with lowest seqnum) that was lost.
 *
 * Return: The number of packets that are presumed to be lost.
 */
static int rdb_check_rtx_queue_acked(struct sock *sk, u32 seq_acked,
				     struct sk_buff **acked,
				     struct sk_buff **lost)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct sk_buff *skb, *tmp, *prev_skb = NULL;
	struct sk_buff *send_head = tcp_send_head(sk);
	struct tcp_skb_cb *scb;
	int fully_acked = true;
	int lost_count = 0;

	tcp_for_write_queue(skb, sk) {
		if (skb == send_head)
			break;

		scb = TCP_SKB_CB(skb);

		/* Determine how many packets and what bytes were acked, no TSO support */
		if (after(scb->end_seq, tp->snd_una)) {
			if (tcp_skb_pcount(skb) == 1 ||
			    !after(tp->snd_una, scb->seq)) {
				break;
			}

			// We do not handle SKBs with gso_segs
			if (tcp_skb_pcount(skb)) {
				break;
			}
			fully_acked = false;
		}

		// Acks up to this packet
		if (scb->end_seq == seq_acked) {
			*acked = skb;
			// This was sent with RDB data, and this SKB acked data on previous skb
			if (RDB_SKB_CB(skb)->rdb_start_seq != 0 && prev_skb) {
				tcp_for_write_queue(tmp, sk) {
					if (tmp == skb) // We have reached the acked SKB
						break;
					lost_count++;
					if (*lost == NULL)
						*lost = skb;
				}
			}
			break;
		}
		if (!fully_acked)
			break;
		prev_skb = skb;
	}
	return lost_count;
}


/**
 * rdb_ack() - Perform loss detection
 * @sk: the socket
 */
void rdb_ack(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct RDBcong *cong = inet_csk_ca(sk);
	struct sk_buff *acked = NULL; // Not actually used atm
	struct sk_buff *lost = NULL;
	int lost_count;

	if (WARN_ON(cong->cc_mode == CC_UNINIT)) {
		trace_printk("rdb_ack: RDB CONG IS NOT INITALIZED. Serious BUG!!\n");
		return;
	}

	lost_count = rdb_check_rtx_queue_acked(sk, tp->snd_una, &acked, &lost);
	if (lost_count) {
		cong->rdb_ack_loss_count += lost_count;
		cong->rdb_ack_loss_count_total += lost_count;
	}

	if (cong->cc_mode == CC_TFRCSP) {
		cong->tfrc.tx_stats->tx_rtt = tp->srtt_us >> 3;
		rdb_tfrc_update_loss_history(&cong->tfrc, sk, lost_count, lost);
	}
	cong->prior_in_flight = tcp_packets_in_flight(tp);
}


/* RFC2861 Check whether we are limited by application or congestion window
 * This is the inverse of cwnd check in tcp_tso_should_defer
 * This is the version from tcp_cong.c in kernel v3.15.
 */
bool tcp_is_cwnd_limited_3_15(const struct sock *sk, u32 in_flight)
{
	const struct tcp_sock *tp = tcp_sk(sk);
	u32 left;
	int sysctl_tcp_tso_win_divisor = 3;

	if (in_flight >= tp->snd_cwnd)
		return true;

	left = tp->snd_cwnd - in_flight;
	if (sk_can_gso(sk) &&
	    left * sysctl_tcp_tso_win_divisor < tp->snd_cwnd &&
	    left < tp->xmit_size_goal_segs)
		return true;
	return left <= tcp_max_tso_deferred_mss(tp);
}


/*
 * TCP Reno congestion control
 * This is special case used for fallback as well.
 */
/* This is Jacobson's slow start and congestion avoidance.
 * SIGCOMM '88, p. 328.
 */
void tcp_reno_cong_avoid_3_15(struct sock *sk, u32 ack, u32 acked)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct RDBcong *cong = inet_csk_ca(sk);

	if (!tcp_is_cwnd_limited_3_15(sk, cong->prior_in_flight))
		return;

	/* In "safe" area, increase. */
	if (tp->snd_cwnd <= tp->snd_ssthresh)
		tcp_slow_start(tp, acked);
	/* In dangerous area, increase slowly. */
	else
		tcp_cong_avoid_ai(tp, tp->snd_cwnd);
}


/**
 * tcp_rdbcong_avoid() - Perform congstion avoidance
 * @sk: the socket
 * @ack: The sequence number that was recently acked
 * @acked: The number of packets newly acked
 */
void tcp_rdbcong_avoid(struct sock *sk, u32 ack, u32 acked)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct RDBcong *cong = inet_csk_ca(sk);

	/*  Update lowest rtt */
	if (tp->srtt_us < cong->minrtt_us) {
		cong->minrtt_us = tp->srtt_us;
	}

	// Packets were registered lost
	if (cong->rdb_ack_loss_count) {
		cong->rdb_ack_loss_count = 0;
		tcp_enter_cwr(sk, true);
	}
	else {
		// Do reno

		trace_printk("IS_CWND_LIMITED: %d, [snd_cwnd < snd_ssthresh (%d < %d: %d) ->  ret: (%d < 2 * %d): %d] else is_cwnd_limited: %d\n",
			     tcp_is_cwnd_limited(sk), tp->snd_cwnd, tp->snd_ssthresh,
			     tp->snd_cwnd <= tp->snd_ssthresh, tp->snd_cwnd, tp->max_packets_out, tp->snd_cwnd < (2 * tp->max_packets_out), tp->is_cwnd_limited);
		//tcp_reno_cong_avoid(sk, ack, acked);
		tcp_reno_cong_avoid_3_15(sk, ack, acked);
	}
}


/**
 * tcp_rdb_ssthresh() - Give the ssthresh to use after slow-start
 * @sk: the socket
 *
 * The slow start threshold is set to a quarter of the congestion
 * window (min 2)
 *
 * Return: the ssthresh to use
 */
u32 tcp_rdb_ssthresh(struct sock *sk)
{
	const struct tcp_sock *tp = tcp_sk(sk);
	return max(tp->snd_cwnd >> 2U, 2U);
}

/**
 * tcp_rdbcong_avoid_tfrc() - Perform congstion avoidance with TFRC CC
 * @sk: the socket
 * @ack: The sequence number that was recently acked
 * @acked: The number of packets newly acked
 */
void tcp_rdbcong_avoid_tfrc(struct sock *sk, u32 ack, u32 acked)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct RDBcong *cong = inet_csk_ca(sk);

	/*  Update lowest rtt */
	if (tp->srtt_us < cong->minrtt_us) {
		cong->minrtt_us = tp->srtt_us;
	}

	// Packets were registered lost
	if (cong->rdb_ack_loss_count) {
		tp->snd_ssthresh = inet_csk(sk)->icsk_ca_ops->ssthresh(sk);
		tp->snd_cwnd = cong->tfrc.tx_stats->tx_x_cwnd;
		tp->snd_cwnd_cnt = 0;
		tp->snd_cwnd_stamp = tcp_time_stamp;
		cong->rdb_ack_loss_count = 0;
	}
	else {
		// Do reno
		tcp_reno_cong_avoid_3_15(sk, ack, acked);
		//tcp_reno_cong_avoid(sk, ack, acked);
	}
	return;
}


/**
 * tcp_rdb_event() - Handle TCP events
 * @sk: the socket
 * @event: The event that was triggered
 */
void tcp_rdb_event(struct sock *sk, enum tcp_ca_event event)
{
	switch (event) {
	case CA_EVENT_FAST_ACK:
		rdb_ack(sk);
		break;

	case CA_EVENT_COMPLETE_CWR:
		break;

	case CA_EVENT_LOSS:
		break;

	case CA_EVENT_SLOW_ACK:
		rdb_ack(sk);
		break;

	default:
		/* don't care */
		break;
	}
}


static inline unsigned int tcp_stream_is_thin_spif(struct tcp_sock *tp)
{
	trace_printk("tcp_stream_is_thin_spif: tp->packets_out < sysctl_tcp_thin_packet_limit: %d, %d < %d, CWND: %d\n",
		     tp->packets_out < sysctl_tcp_thin_packet_limit, tp->packets_out, sysctl_tcp_thin_packet_limit, tp->snd_cwnd);
	return tp->packets_out < sysctl_tcp_thin_packet_limit;
}

/**
 * tcp_stream_is_thin_dpif() - Tests if the stream is thin based in dynamic PIF limit
 * @tp: the tcp_sock struct
 * @min_rtt_us: The minimun RTT registered for the connection
 *
 * Return: true if current packet in flight (PIF) count is lower than
 * the dynamic PIF limit, else false
 */
inline inline bool tcp_stream_is_thin_dpif(struct tcp_sock *tp, u32 min_rtt_us)
{
	// Div by 1000 to get ms,
	// Div by dynamic_pif, the minimum allowed ITT (Inter-transmission time) in ms
	return tp->packets_out < ((min_rtt_us >> 3) / 1000 / dynamic_pif);
}


/**
 * run_rdb() - Tests if we can do RDB
 * @cong: The RDBCong struct for the connection
 * @tp: the tcp_sock struct
 *
 * Return: true if we can do RDB, else false
 */
static inline bool run_rdb(struct RDBcong *cong, struct tcp_sock *tp)
{
	return (tp->thin_rdb) &&
		(cong->dynamic_pif ? tcp_stream_is_thin_dpif(tp, cong->minrtt_us)
		 : tcp_stream_is_thin_spif(tp));
}


/**
 * do_rdb() - Try to create and send an RDB packet
 * @xmit_skb: The original SKB to be sent
 * @mss_now: Current MSS
 * @sk: the socket
 * @tp: the tcp_sock struct
 * @gfp_mask: The gfp_t allocation
 * @loop_count: The loop iteration index
 * @retrans: If this is a retransmit
 *
 * Return: true if successfully sent RDB packet, else false
 */
bool do_rdb(struct sk_buff *xmit_skb, unsigned int mss_now,
	    struct sock *sk, struct tcp_sock *tp, gfp_t gfp_mask,
	    int loop_count, bool retrans)
{
	struct sk_buff *rdb_skb = NULL;
	struct inet_connection_sock *icsk;
	struct RDBcong *cong = inet_csk_ca(sk);
	unsigned int rdb_len = 0;
	bool ret = false;

	if (WARN_ON(cong->cc_mode == CC_UNINIT)) {
		trace_printk("do_rdb: RDB CONG IS NOT INITALIZED. Serious BUG!!\n");
		return ret;
	}

	// Retrans not implemented
	if (retrans)
		goto end;

	// How we detect that RDB was used. This is a hack!, and needs to be solved some other way.
	RDB_SKB_CB(xmit_skb)->rdb_start_seq = 0;

	if (run_rdb(cong, tp)) {

		rdb_skb = redundant_data_bundling(xmit_skb, mss_now, sk, gfp_mask, retrans);

		if (!rdb_skb)
			goto end;

		icsk = inet_csk(sk);
		// Set tstamp for SKB in output queue. (because tcp_transmit_skb will
		// do this for the rdb skb and not the skb in the output queue)
		skb_mstamp_get(&xmit_skb->skb_mstamp);
		rdb_skb->tstamp.tv64 = 0;

		// tcp_transmit_skb returns 0 on success
		ret = !tcp_transmit_skb(sk, rdb_skb, 0, gfp_mask);
	}
end:;

	if (cong->cc_mode == CC_TFRCSP) {
		// Use the original SKB
		rdb_len = xmit_skb->len;

		if (ret) {
			// We use the RDB SKB
			RDB_BUG_ON(rdb_skb == NULL);
			rdb_len = rdb_skb->len;
		}
		rdb_tx_update_s(&cong->tfrc, xmit_skb->len, rdb_len);
		cong->tfrc.tx_stats->tx_t_last_sent = ktime_get_real();
		cong->tfrc.tx_stats->tx_bytes_sent += rdb_len;
	}
	return ret;
}
