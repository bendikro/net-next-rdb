/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2014, Bendik Rønning Opstad <bendikro@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * The TFRC code is based on the TFRC implemention from net/dccp/ccids/
 * and customized for RDB.
 */

#ifndef _RDB_EQUATION_H_
#define _RDB_EQUATION_H_
/*
 *  Copyright (c) 2007   The University of Aberdeen, Scotland, UK
 *  Copyright (c) 2005-6 The University of Waikato, Hamilton, New Zealand.
 *  Copyright (c) 2005-6 Ian McDonald <ian.mcdonald@jandi.co.nz>
 *  Copyright (c) 2005   Arnaldo Carvalho de Melo <acme@conectiva.com.br>
 *  Copyright (c) 2003   Nils-Erik Mattsson, Joacim Haggmark, Magnus Erixzon
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 */
#include <linux/types.h>
#include <linux/math64.h>
#include <linux/kernel.h>
#include <linux/kernel.h>
#include <linux/tcp.h>
#include <net/tcp.h>

/*
 * 	RDB - specific warning and debugging macros.
 */
#define RDB_WARN(fmt, a...) LIMIT_NETDEBUG(KERN_WARNING "%s: " fmt,       \
							__func__, ##a)
#define RDB_CRIT(fmt, a...) printk(KERN_CRIT fmt " at %s:%d/%s()\n", ##a, \
					 __FILE__, __LINE__, __func__)
#define RDB_BUG(a...)       do { RDB_CRIT("BUG: " a); dump_stack(); } while(0)
#define RDB_BUG_ON(cond)    do { if (unlikely((cond) != 0))		   \
				     RDB_BUG("\"%s\" holds (exception!)", \
					      __stringify(cond));          \
			     } while (0)


/* integer-arithmetic divisions of type (a * 1000000)/b */
static inline u64 scaled_div(u64 a, u64 b)
{
	BUG_ON(b == 0);
	return div64_u64(a * 1000000, b);
}

static inline u32 scaled_div32(u64 a, u64 b)
{
	u64 result = scaled_div(a, b);

	if (result > UINT_MAX) {
		RDB_CRIT("Overflow: %llu/%llu > UINT_MAX",
			  (unsigned long long)a, (unsigned long long)b);
		return UINT_MAX;
	}
	return result;
}

/**
 * tfrc_ewma  -  Exponentially weighted moving average
 * @weight: Weight to be used as damping factor, in units of 1/10
 */
static inline u32 tfrc_ewma(const u32 avg, const u32 newval, const u8 weight)
{
	return avg ? (weight * avg + (10 - weight) * newval) / 10 : newval;
}

extern u32  tfrc_calc_x(u16 s, u32 R, u32 p);
extern u32  tfrc_calc_x_reverse_lookup(u32 fvalue);
extern u32  tfrc_invert_loss_event_rate(u32 loss_event_rate);

#endif /* _RDB_EQUATION_H_ */
