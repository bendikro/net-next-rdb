/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2012-2015, Bendik Rønning Opstad <bro.devel+kernel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _RDB_SKB_H
#define _RDB_SKB_H

#include <net/tcp.h>

#define TCP_RDB_MAX_BUNDLE_BYTES      0 /* Thin streams: Limit maximum bundled bytes */

#define FLAG_DATA		0x01 /* Incoming frame contained data.		*/
#define FLAG_WIN_UPDATE		0x02 /* Incoming ACK was a window update.	*/
#define FLAG_DATA_ACKED		0x04 /* This ACK acknowledged new data.		*/
#define FLAG_RETRANS_DATA_ACKED	0x08 /* "" "" some of which was retransmitted.	*/
#define FLAG_SYN_ACKED		0x10 /* This ACK acknowledged SYN.		*/
#define FLAG_DATA_SACKED	0x20 /* New SACK.				*/
#define FLAG_ECE		0x40 /* ECE in this ACK				*/
#define FLAG_SLOWPATH		0x100 /* Do not skip RFC checks for window update.*/
#define FLAG_ONLY_ORIG_SACKED	0x200 /* SACKs only non-rexmit sent before RTO */
#define FLAG_SND_UNA_ADVANCED	0x400 /* Snd_una was changed (!= FLAG_DATA_ACKED) */
#define FLAG_DSACKING_ACK	0x800 /* SACK blocks contained D-SACK info */
#define FLAG_NONHEAD_RETRANS_ACKED	0x1000 /* Non-head rexmitted data was ACKed */
#define FLAG_SACK_RENEGING	0x2000 /* snd_una advanced to a sacked seq */

#define FLAG_ACKED		(FLAG_DATA_ACKED|FLAG_SYN_ACKED)
#define FLAG_NOT_DUP		(FLAG_DATA|FLAG_WIN_UPDATE|FLAG_ACKED)
#define FLAG_CA_ALERT		(FLAG_DATA_SACKED|FLAG_ECE)
#define FLAG_FORWARD_PROGRESS	(FLAG_ACKED|FLAG_DATA_SACKED)
#define FLAG_ANY_PROGRESS	(FLAG_FORWARD_PROGRESS|FLAG_SND_UNA_ADVANCED)


struct sk_buff* redundant_data_bundling(struct sk_buff *skb, unsigned int mss_now, struct sock *sk, gfp_t gfp_mask, bool retrans);

extern void print_stream_separator(struct sock *sk);
extern void print_char(char c, int count);
extern void print_stats(struct tcp_sock *tp, struct sk_buff *skb, __u32 rdb, int retrans, int number_in_queue);
extern void print_skb(char *prefix, struct sk_buff *skb);
extern void rdb_ack_accounting(struct sock *sk, const struct sk_buff *skb, int flag, u32 prior_snd_una, u32 prior_fackets);
extern void print_skb_data_as_string(char *prefix, struct sk_buff *skb);
extern void print_skb(char *prefix, struct sk_buff *skb);

extern int tcp_transmit_skb(struct sock *sk, struct sk_buff *skb, int clone_it,
							gfp_t gfp_mask);

extern void copy_skb_header(struct sk_buff *new,
				   const struct sk_buff *old);

struct rdb_skb_cb {
	struct tcp_skb_cb tcp_cb;
	__u32		rdb_start_seq;	/* Start seq of rdb data bundled on last transmit */
};
#define RDB_SKB_CB(__skb)	((struct rdb_skb_cb *)&((__skb)->cb[0]))

#define tcp_for_write_queue_reverse_from_safe(skb, tmp, sk)				\
	skb_queue_reverse_walk_from_safe(&(sk)->sk_write_queue, skb, tmp)

#endif	/* _RDB_SKB_H */
