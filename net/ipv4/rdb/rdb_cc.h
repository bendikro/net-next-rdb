/*
 * Implementation of Redundant Data Bundling (RDB) in TCP.
 *
 * Copyright (C) 2012-2015, Bendik Rønning Opstad <bro.devel+kernel@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _RDB_CC_H
#define _RDB_CC_H

#include "lib/rdb_tfrc.h"

enum RDB_CC_MODE { CC_UNINIT, CC_DEFAULT, CC_TFRCSP };

/* RDB variables */
struct RDBcong {
	u32 rdb_ack_loss_count;	         /* Number of packets registered lost by RDB since last cwnd update */
	u32 rdb_ack_loss_count_total;    /* Number of packets registered lost by RDB */
	u32 minrtt_us;                   /* Minimum smoothed round trip time value seen */
	u32 prior_in_flight;
	u8 cc_mode;
	u8 dynamic_pif;
	struct rdb_tfrc tfrc;
};


void rdb_cong_init(struct sock *sk);
void rdb_cong_release(struct sock *sk);
void rdb_cong_tfrc_init(struct sock *sk);
void rdb_cong_tfrc_release(struct sock *sk);

u32 tcp_rdb_ssthresh(struct sock *sk);
void tcp_rdbcong_avoid(struct sock *sk, u32 ack, u32 acked);
void tcp_reno_cong_avoid_3_15(struct sock *sk, u32 ack, u32 acked);
bool do_rdb(struct sk_buff *xmit_skb, unsigned int mss_now,
		   struct sock *sk, struct tcp_sock *tp, gfp_t gfp_mask,
		   int loop_count, bool retrans);
void tcp_rdbcong_avoid_tfrc(struct sock *sk, u32 ack, u32 acked);
void tcp_rdb_event(struct sock *sk, enum tcp_ca_event event);

#endif	/* _RDB_CC_H */
